class Player:
	
	#each player has $500 at start
	balance = 500
	cards = []
	total = 0
	
	def add(self, bonus):
		self.balance = self.balance + bonus
		
	def subtract(self, loss):
		self.balance = self.balance - loss
		
	def add_to_cards(self, new_card):
		self.cards.append(new_card)
		
	def clear_cards(self):
		self.cards.clear()
		
	def set_total(self):
		self.clear_total()
		for card in self.cards:
			self.total = self.total + card.card_val
			
	def clear_total(self):
		self.total = 0
		
	def refresh(self):
		self.cards.clear()
