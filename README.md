Project name: Simple Blackjack

Project author: Chigozie Davis

Description:

Simple Blackjack is a single-player command-line based blackjack game that 
uses the basic rules of the card game Blackjack. The player starts with $500, 
and can place bets each round as the game goes on. The player can choose to end 
the game at the end of any round, and the game is automatically over when the 
player has no money to bet. Like in Blackjack, the player can choose to "hit" or 
"stand" after getting their initial two cards from the dealer. If the player or 
dealer go over 21, they go "bust" and lose that round. Each regular card is has 
a value of whatever number is on the card, and the King, Queen, and Jack each 
have a value of 10. The ace usually has a value of 11, but the player has the 
option of changing its value to a 1. Profits are 1-1, so the player if a player 
bets $100 at the start of the hand and wins, the player receives $200.
    I made this game to refresh my python programming skills as I had not written 
Python code in a while.