class Card:
	
	card_type = ""
	card_val = 0
	
	def __init__(self, card_num):
		self.card_num = card_num
		
	def __str__(self):
		return f"You have the {self.card_num} of {self.card_type}s"
		
	def set_value(self):
		if(self.card_num == 'Ace'):
			self.card_val = 11
		elif(self.card_num == 'Jack'):
			self.card_val = 10
		elif(self.card_num == 'Queen'):
			self.card_val = 10
		elif(self.card_num == 'King'):
			self.card_val = 10
		else:
			self.card_val = int(self.card_num)
		
class Club(Card):
	
	def __init__(self, card_num):
		Card.__init__(self,card_num)
		self.card_type = 'Club'
		Card.set_value(self)
		
		
class Heart(Card):
	
	def __init__(self, card_num):
		Card.__init__(self,card_num)
		self.card_type = 'Heart'
		Card.set_value(self)
		
class Spade(Card):
	
	def __init__(self, card_num):
		Card.__init__(self,card_num)
		self.card_type = 'Spade'
		Card.set_value(self)
		
class Diamond(Card):
	
	def __init__(self, card_num):
		Card.__init__(self,card_num)
		self.card_type = 'Diamond'
		Card.set_value(self)
