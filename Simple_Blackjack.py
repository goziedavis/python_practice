import random
from Deck import Card
from Player import Player
	
def create_deck():
	
	deck = []
	
	#create clubs
	for x in range (0,13):
		if(x > 0 and x < 10):
			new_club = Card.Club(str(x+1))
			deck.append(new_club)
		if(x == 0):
			new_club = Card.Club('Ace')
			deck.append(new_club)
		if(x == 10):
			new_club = Card.Club('Jack')
			deck.append(new_club)
		if(x == 11):
			new_club = Card.Club('Queen')
			deck.append(new_club)
		if(x == 12):
			new_club = Card.Club('King')
			deck.append(new_club)
			
	#create diamonds
	for x in range (0,13):
		if(x > 0 and x < 10):
			new_diamond = Card.Diamond(str(x+1))
			deck.append(new_diamond)
		if(x == 0):
			new_diamond = Card.Diamond('Ace')
			deck.append(new_diamond)
		if(x == 10):
			new_diamond = Card.Diamond('Jack')
			deck.append(new_diamond)
		if(x == 11):
			new_diamond = Card.Diamond('Queen')
			deck.append(new_diamond)
		if(x == 12):
			new_diamond = Card.Diamond('King')
			deck.append(new_diamond)
			
	#create hearts
	for x in range (0,13):
		if(x > 0 and x < 10):
			new_heart = Card.Heart(str(x+1))
			deck.append(new_heart)
		if(x == 0):
			new_heart = Card.Heart('Ace')
			deck.append(new_heart)
		if(x == 10):
			new_heart = Card.Heart('Jack')
			deck.append(new_heart)
		if(x == 11):
			new_heart = Card.Heart('Queen')
			deck.append(new_heart)
		if(x == 12):
			new_heart = Card.Heart('King')
			deck.append(new_heart)
			
	#create spades
	for x in range (0,13):
		if(x > 0 and x < 10):
			new_spade = Card.Spade(str(x+1))
			deck.append(new_spade)
		if(x == 0):
			new_spade = Card.Spade('Ace')
			deck.append(new_spade)
		if(x == 10):
			new_spade = Card.Spade('Jack')
			deck.append(new_spade)
		if(x == 11):
			new_spade = Card.Spade('Queen')
			deck.append(new_spade)
		if(x == 12):
			new_spade = Card.Spade('King')
			deck.append(new_spade)
	
	return deck

def check_integer(amount):
	
	try:
		check_string = int(amount)
	except:
		return False
		
	return True
	
def start_msg():
	
	print("\033[1;31;40m---------------------------------------------------------------")
	print("\033[1;31;40m----------------SIMPLE ONE PLAYER BLACKJACK--------------------")
	print("\033[1;31;40m--------------------by Chigozie Davis--------------------------")
	print("\033[1;31;40m---------------------------------------------------------------")
	print("\033[0;37;40m")

def main():
	
	start_msg()
	
	new_deck = []
	used_deck = []
	checked_deck = []
	count = 0
	
	#create new player and dealer
	player = Player.Player()
	dealer = Player.Player()
	
	#start game
	while True:
		#create new deck
		new_deck.clear()
		used_deck.clear()
		checked_deck.clear()
		new_deck = create_deck()
		
		player.refresh()
		dealer.refresh()
		
		#display current balance
		print(f"Current player balance: ${player.balance}\n")
		
		#if balance is zero, game over
		if(player.balance <= 0):
			print(f"Your current balance is ${player.balance}, so you are unable to place a bet. Game over.")
			break
		
		if(count > 0):
			proceed_option = input("Do you wish to continue playing (Yes/No): ")
			while(proceed_option != 'Yes' and proceed_option != 'No'):
				proceed_option = input("Do you wish to continue playing (Yes/No): ")
			if(proceed_option == 'Yes'):
				pass
			elif(proceed_option == 'No'):
				break
				
		count = 1
		
		#player places bet
		player_bet = input("How much do you want to bet: ")
		while(check_integer(player_bet) != True or int(player_bet) > player.balance):
			if(check_integer(player_bet) != True):
				print("Please enter an integer amount\n")
			else:
				print(f"You currently have ${player.balance}. Place a bet within that range.\n")
			player_bet = input("How much do you want to bet: ")
		
		#dealer deals 2 cards
		print("The dealer deals you two cards...\n")
		player.cards = random.choices(new_deck, k=2)
		for card in player.cards:
			used_deck.append(card)
		
		#print users cards
		for card in player.cards:
			print(card)
			
		#check if user has ace(s)
		for card in player.cards:
			if(card.card_num == 'Ace' and card not in checked_deck):
				checked_deck.append(card)
				ace_value = input("You have an ace. Do you wish to change its value from 11 to 1? (Yes/No): ")
				while(ace_value != 'Yes' and ace_value != 'No'):
					ace_value = input("You have an ace. Do you wish to change its value from 11 to 1? (Yes/No): ")
					
				if(ace_value == 'Yes'):
					card.card_val = 1
			
		#check user total
		player.set_total()
		print(f"Your current total is {player.total}.")
		
		#if total is less than 21, ask user if they want to stand or hit
		while(player.total < 21):
			user_selection = input("Do you wish to stand or hit: ")
			while(user_selection != 'stand' and user_selection != 'hit'):
				user_selection = input("Do you wish to stand or hit: ")
			
			if(user_selection == 'hit'):
				new_card = random.choice(new_deck)
				while(new_card in used_deck):
					new_card = random.choice(new_deck)
				used_deck.append(new_card)
				player.add_to_cards(new_card)
				player.set_total()
				print()
				
				#print player's current cards
				for cards in player.cards:
					print(cards)
					
				#check if user has ace(s)
				for card in player.cards:
					if(card.card_num == 'Ace'):
						ace_value = input("You have an ace. Do you wish to change its value from 11 to 1? (Yes/No)")
						while(ace_value != 'Yes' and ace_value != 'No'):
							ace_value = input("You have an ace. Do you wish to change its value from 11 to 1? (Yes/No)")
							
						if(ace_value == 'Yes'):
							card.card_val = 1
							
				print(f"Your current total is {player.total}.")
				
			elif(user_selection == 'stand'):
				break
				
		#if total is greater than 21, player goes bust
		if(player.total > 21):
			print("You have gone bust, and lost your bet")
			player.subtract(int(player_bet))
			continue
		
		#dealer now gets a card
		print()
		print("Dealer gets a card now...")
		
		dealer_card = random.choice(new_deck)
		while(dealer_card in used_deck):
			dealer_card = random.choice(new_deck)
		used_deck.append(dealer_card)
		dealer.add_to_cards(dealer_card)
		dealer.set_total()
		print("Dealer's cards are:")
		for card in dealer.cards:
			print(f"{card.card_val} of {card.card_type}")
		print(f"Dealer's total is {dealer.total}.")
		
		while(dealer.total <= 16):
			print()
			print("Dealer gets another card now...")
			new_card = random.choice(new_deck)
			while(new_card in used_deck):
				new_card = random.choice(new_deck)
			used_deck.append(new_card)
			dealer.add_to_cards(new_card)
			dealer.set_total()
			print("Dealer's cards are:")
			for card in dealer.cards:
				print(f"{card.card_val} of {card.card_type}")
			print(f"Dealer's total is {dealer.total}.")
		
		#if the dealer's total is greater than 21, the dealer goes bust	
		if(dealer.total > 21):
			print("The dealer has gone bust, and you have won this bet")
			player.add(int(player_bet))
			continue
			
		if(dealer.total < player.total):
			print("You have a higher hand than the dealer. You win this bet")
			player.add(int(player_bet))
		elif(dealer.total == player.total):
			print("You have the same total as the dealer. No one wins this bet")
		else:
			print("You have a lesser hand than the dealer. You lose this bet")
			player.subtract(int(player_bet))
	
if __name__ == "__main__":
	main()
